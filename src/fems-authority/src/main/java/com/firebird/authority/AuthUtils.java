package com.firebird.authority;

import javax.servlet.http.HttpSession;

import com.firebird.authority.domain.Organization;
import com.firebird.authority.service.UserServiceInter;
import com.firebird.core.auth.domain.LoginUser;
import com.firebird.util.spring.BeanFactory;
import com.firebird.web.constant.FirebirdConstant;

public class AuthUtils {
	public static Organization getCurrentOrganization(HttpSession session) {
		UserServiceInter users = (UserServiceInter) BeanFactory.getBean("userServiceImpl");
		LoginUser user = (LoginUser) session.getAttribute(FirebirdConstant.SESSION_USEROBJ);
		return users.getOrg(user.getId());
	}
}
