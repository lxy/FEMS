var fekp_latex_editor;
var fekplatexKnowdialog;
KindEditor.plugin('fekplatex', function(K) {
	fekp_latex_editor = this, name = 'fekplatex';
	// 点击图标时执行
	fekp_latex_editor.clickToolbar(name, function() {
		fekplatexKnowdialog = K
		.dialog( {
			width : 400,
			title : '插入LaTeX公式',
			body : '<div style="margin:10px;height:250px;text-align: center;overflow:auto;">'+
			       '<div id="kindEditorLatexBoxId">loading...</div>'+
				   '</div>',
			closeBtn : {
				name : '关闭',
				click : function(e) {
					fekplatexKnowdialog.remove();
				}
			}
		});
		$('#kindEditorLatexBoxId').load('latex/inputcom.do',{},function(){
			$('#kindEditorLatexBoxId #insertLatexImg').click(function(){
				if ($('#codeId').val()) {
					$('#latexloadTitleId').text('公式解析中......');
					$.post('latex/latexpng.do', {
						latex : $('#codeId').val()
					}, function(flag) {
						if(flag.error==0){
							//拼装img
							var tag='<img height="60"  src="'+flag.url+'" alt="" />';
							//插入img
							fekp_latex_editor.insertHtml(tag);
							fekplatexKnowdialog.remove();
						}else{
							$('#latexloadTitleId').text(flag.message);
						}
					}, 'json');
				} else {
					$('#latexloadTitleId').text('请输入公式脚本...');
				}
			});
		});
	});
});
KindEditor.lang({
	fekplatex : '插入公式'
});