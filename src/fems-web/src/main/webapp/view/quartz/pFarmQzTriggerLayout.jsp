<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/firebirdtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>触发器定义</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="dom_searchfirebirdqztrigger">
			<table class="editTable">
				<tr>
					<td class="title">名称:</td>
					<td><input name="NAME:like" type="text"></td>
					<td class="title"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a></td>
					<td><a id="a_reset" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table class="easyui-datagrid" id="dom_datagridfirebirdqztrigger">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="80">名称</th>
					<th field="DESCRIPT" data-options="sortable:true" width="80">
						脚本</th>
					<th field="PCONTENT" data-options="sortable:true" width="80">
						备注</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionfirebirdqztrigger = "qzTrigger/del.do";
	var url_formActionfirebirdqztrigger = "qzTrigger/form.do";
	var url_searchActionfirebirdqztrigger = "qzTrigger/query.do";
	var title_windowfirebirdqztrigger = "触发器定义";
	var gridfirebirdqztrigger;
	var searchfirebirdqztrigger;
	var TOOL_BARfirebirdqztrigger = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDatafirebirdqztrigger
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDatafirebirdqztrigger
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDatafirebirdqztrigger
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDatafirebirdqztrigger
	} ];
	$(function() {
		gridfirebirdqztrigger = $('#dom_datagridfirebirdqztrigger').datagrid({
			url : url_searchActionfirebirdqztrigger,
			fit : true,
			fitColumns : true,
			'toolbar' : TOOL_BARfirebirdqztrigger,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			striped : true,
			rownumbers : true,
			ctrlSelect : true,
			fitColumns : true
		});
		searchfirebirdqztrigger = $('#dom_searchfirebirdqztrigger').searchForm({
			gridObj : gridfirebirdqztrigger
		});
	});
	function viewDatafirebirdqztrigger() {
		var selectedArray = $(gridfirebirdqztrigger).datagrid('getSelections');
		if (selectedArray.length > 0) {
			var url = url_formActionfirebirdqztrigger + '?operateType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.firebird.openWindow({
				id : 'winfirebirdqztrigger',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	function addDatafirebirdqztrigger() {
		var url = url_formActionfirebirdqztrigger + '?operateType=' + PAGETYPE.ADD;
		$.firebird.openWindow({
			id : 'winfirebirdqztrigger',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	function editDatafirebirdqztrigger() {
		var selectedArray = $(gridfirebirdqztrigger).datagrid('getSelections');
		if (selectedArray.length > 0) {
			var url = url_formActionfirebirdqztrigger + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			;
			$.firebird.openWindow({
				id : 'winfirebirdqztrigger',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	function delDatafirebirdqztrigger() {
		var selectedArray = $(gridfirebirdqztrigger).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$.post(url_delActionfirebirdqztrigger + '?ids='
							+ $.firebird.getCheckedIds(gridfirebirdqztrigger), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								if (jsonObject.STATE == 0) {
									$(gridfirebirdqztrigger).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>




