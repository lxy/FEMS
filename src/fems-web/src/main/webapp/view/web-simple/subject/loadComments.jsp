<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.firebird.web.constant.FirebirdConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/firebirdtag.tld" prefix="PF"%>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<div class="row">
	<div class="col-md-12">
		<jsp:include page="commons/includeComments.jsp"></jsp:include>
		<jsp:include page="commons/includeCommentsForm.jsp"></jsp:include>
	</div>
</div>
