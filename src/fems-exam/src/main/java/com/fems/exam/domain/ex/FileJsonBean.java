package com.fems.exam.domain.ex;

import com.firebird.doc.domain.FirebirdDocfile;

public class FileJsonBean implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2165169049243720411L;
	private FirebirdDocfile info;
	private String base64;

	public FirebirdDocfile getInfo() {
		return info;
	}

	public void setInfo(FirebirdDocfile info) {
		this.info = info;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}
}
