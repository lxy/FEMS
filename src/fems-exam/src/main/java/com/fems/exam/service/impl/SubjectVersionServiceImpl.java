package com.fems.exam.service.impl;

import com.fems.exam.domain.SubjectVersion;
import com.firebird.doc.server.FirebirdFileManagerInter;
import com.firebird.doc.server.FirebirdFileManagerInter.FILE_APPLICATION_TYPE;

import org.apache.log4j.Logger;
import com.fems.exam.dao.SubjectVersionDaoInter;
import com.fems.exam.service.SubjectVersionServiceInter;
import com.firebird.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.firebird.core.auth.domain.LoginUser;

/* *
 *功能：考题版本服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FirebirdCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class SubjectVersionServiceImpl implements SubjectVersionServiceInter {
	@Resource
	private SubjectVersionDaoInter subjectversionDaoImpl;
	@Resource
	private FirebirdFileManagerInter firebirdFileManagerImpl;
	private static final Logger log = Logger.getLogger(SubjectVersionServiceImpl.class);

	@Override
	@Transactional
	public SubjectVersion insertSubjectversionEntity(SubjectVersion entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		entity = subjectversionDaoImpl.insertEntity(entity);
		// --------------------------------------------------
		firebirdFileManagerImpl.submitFileByAppHtml(entity.getTipnote(), entity.getId(), FILE_APPLICATION_TYPE.SUBJECTNOTE);
		return entity;
	}

	@Override
	@Transactional
	public void deleteSubjectversionEntity(String id, LoginUser user) {
		subjectversionDaoImpl.deleteEntity(subjectversionDaoImpl.getEntity(id));
		firebirdFileManagerImpl.cancelFilesByApp(id);
	}

	@Override
	@Transactional
	public SubjectVersion getSubjectversionEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return subjectversionDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createSubjectversionSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "FEMS_SUBJECT_VERSION",
				"ID,TIPTYPE,SUBJECTID,TIPNOTE,TIPSTR,PCONTENT,PSTATE,CUSER,CUSERNAME,CTIME");
		return dbQuery;
	}

}
