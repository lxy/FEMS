package com.firebird.quartz.server;

import java.text.ParseException;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;

import com.firebird.quartz.domain.FirebirdQzScheduler;
import com.firebird.quartz.domain.FirebirdQzTask;
import com.firebird.quartz.domain.FirebirdQzTrigger;

/**
 * queatz的转化接口
 * 
 * @author Administrator
 * 
 */
public interface QuartzInter {
	public JobDetail getJobDetail(FirebirdQzScheduler node,FirebirdQzTask task) throws ClassNotFoundException;
	public JobDetail getJobDetail(String schedulerId,FirebirdQzTask task) throws ClassNotFoundException;
	public Trigger getTrigger(FirebirdQzScheduler node,FirebirdQzTrigger trigger) throws ParseException;
	public JobKey getJobKey(FirebirdQzScheduler node,FirebirdQzTask task);
}
