package com.firebird.quartz.server.impl;

import static org.quartz.JobBuilder.newJob;

import java.text.ParseException;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.quartz.impl.triggers.CronTriggerImpl;

import com.firebird.quartz.domain.FirebirdQzScheduler;
import com.firebird.quartz.domain.FirebirdQzTask;
import com.firebird.quartz.domain.FirebirdQzTrigger;
import com.firebird.quartz.server.QuartzInter;

public class QuartzImpl implements QuartzInter {

	@Override
	public JobDetail getJobDetail(FirebirdQzScheduler node, FirebirdQzTask task)
			throws ClassNotFoundException {
		return getJobDetail(node.getId(), task);
	}

	@SuppressWarnings("deprecation")
	@Override
	public Trigger getTrigger(FirebirdQzScheduler node, FirebirdQzTrigger trigger)
			throws ParseException {
		Trigger qtrigger = new CronTriggerImpl(node.getId() + trigger.getId(),
				null, trigger.getDescript().trim());
		return qtrigger;
	}

	@Override
	public JobKey getJobKey(FirebirdQzScheduler node, FirebirdQzTask task) {
		return new JobKey(node.getId() + task.getId());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public JobDetail getJobDetail(String schedulerId, FirebirdQzTask task)
			throws ClassNotFoundException {
		Class cla = Class.forName(task.getJobclass().trim());
		JobDataMap dataMap = new JobDataMap();
		dataMap.put("PARA", task.getJobparas());
		JobDetail job = newJob(cla).withIdentity(schedulerId + task.getId())
				.setJobData(dataMap).build();
		return job;
	}

}
