package com.firebird.util.cache;

/**
 * 缓存名称
 * 
 * @author macpl
 *
 */
public enum FirebirdCacheName {
	// 知识缓存
	DocCache("fekp-cache-docs"),
	// 知识分类缓存（用于sevice中获得知识的分类一般缓存设置于1秒之内）
	DocTypeCache("fekp-cache-doctype"),
	//获得专家的userids
	ExperUserIds("fekp-expert-userids"),
	// 附件緩存
	FileCache("fekp-cache-files"),
	// 分享鏈接緩存1
	ShareLevel1Cache("fekp-share-level1"),
	// 分享鏈接緩存2
	ShareLevel2Cache("fekp-share-level2"),
	// 分享鏈接緩存3
	ShareLevel3Cache("fekp-share-level3");
	/**
	 * 持久缓存
	 */
	private String permanentCacheName;

	FirebirdCacheName(String permanentCacheName) {
		this.permanentCacheName = permanentCacheName;
	}

	/**
	 * 如果只有一个缓存就是这个持久缓缓存
	 * 
	 * @return
	 */
	public String getPermanentCacheName() {
		return permanentCacheName;
	}
}
